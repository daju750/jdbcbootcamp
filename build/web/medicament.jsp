
<%@page import="svc.ProviderDto"%>
<%@page import="dao.ProviderServiceImpl"%>
<%@page import="svc.MedicamentDto"%>
<%@page import="dao.MedicamentServiceImpl"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>
<html

<head>
    <meta charset="UTF-8">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <title>Crud Medicamentos</title>

</head>

<body>
    
    <div class="container">

        <center><h5>Editar Medicamento</h5></center><br>
        <form class="col s12" action = "medicament" method="POST">

            <div class="row">
                
                <%
                MedicamentServiceImpl medicamentService = new MedicamentServiceImpl();
                MedicamentDto medicament = medicamentService.findById(Integer.parseInt(request.getParameter("id")));
                %>
                
                <div class="input-field col s6">
                     <label for="name">Nombre</label> 
                     <input type="text" name="name" value="<%=medicament.getName() %>" id="nombre">
                </div>
                
                <div class="input-field col s6">
                    <select name="provider" class="browser-default">
                            <%
                            ProviderServiceImpl providerService = new ProviderServiceImpl();
                             List<ProviderDto> providerList = providerService.orderBy(medicament.getProvider());
                            for(ProviderDto provider: providerList) {
                            %>            
                            <option value="<%=provider.getId() %>"><%=provider.getName() %></option>    
                            <% } %>
                    </select>
                </div>

            </div>

                <div class="row">
                    <div class="input-field col s6">
                        <label for="price">Precio</label> 
                        <input type="number" name="price" value="<%=medicament.getPrice() %>" id="price">
                    </div>
                    <input type="hidden" id="id" name="id" value="<%=request.getParameter("id") %>">
                    <div class="input-field col s6">
                        <label for="unity">Unidades</label> 
                    <input type="number" name="unity" value="<%=medicament.getUnity() %>" id="unity">
                    </div>
                </div>

            <button class="btn waves-effect waves-light" type="submit">Guardar</button>

        </form>

    </div>			

</body>

</html>
