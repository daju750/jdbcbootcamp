
<%@page import="dao.MedicamentProviderServiceImpl"%>
<%@page import="svc.InfoMedicaments"%>
<%@page import="svc.Pagination"%>
<%@page import="svc.Philtre"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Crud Medicamentos</title>
    </head>
    <body>
  
        <div class="container">
            <center><h5>Catalogo Medicamentos</h5></center><br>
            <br>
                <a href="form-medicament.jsp" class="btn-floating btn-large waves-effect waves-light red">
                    <i class="material-icons">add</i>
                </a>
	<br>
            <table class="responsive-table">
                <thead>
                    <tr>
                        <th>Proveedor</th>
                        <th>Medicamento</th>
                        <th>Fecha Creacion</th>
                        <th>Precio</th>
                        <th>Unidades</th>
                        <th>Eliminar</th>
                        <th>Modificar</th>
                    </tr>
                </thead>
                <tbody>  
                <%
                Philtre filter = new Philtre(Arrays.asList(1, 2, 3));
                Pagination pagination = new Pagination(10,0);
                List<InfoMedicaments> infoMedicaments = 
                        new MedicamentProviderServiceImpl().find(pagination, filter);
                for(InfoMedicaments medicament: infoMedicaments){
                %>
                    <tr>
                        <td><%=medicament.getProvider() %></td>
                        <td><%=medicament.getName() %></td>
                        <td><%=medicament.getCreationDate() %></td>
                        <td><%=medicament.getPrice() %></td>
                        <td><%=medicament.getUnity() %></td>
                        <td>  
                            <a href="/ciam/medicaments-provider?id=<%=medicament.getMedicamentId() %>"
                               onclick="return confirm('Quiere Borrar?')">
                                <i class="tiny material-icons red-text text-darken-1">delete</i>
                            </a>Borrar
                        </td>
                        <td>
                            <a href="/ciam/medicament.jsp?id=<%=medicament.getMedicamentId() %>">
                                <i class="tiny material-icons green-text text-darken-1">save</i>
                            </a>Editar
                        </td>
                    </tr>
                <% } %>
                </tbody>
            </table>
        <div>
    
    </body>
</html>