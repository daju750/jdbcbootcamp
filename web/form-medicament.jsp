<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>
<html

<head>
    <meta charset="UTF-8">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Crud Medicamentos</title>
</head>

<body>
    
    <div class="container">

        <center><h5>Nuevo Medicamento</h5></center><br>
        <form class="col s12" action = "medicaments-provider" method="POST">

            <div class="row">

                <div class="input-field col s6">
                     <label for="name">Nombre</label> 
                 <input type="text" name="name" id="nombre">
                </div>

                <div class="input-field col s6">
                    <select name="provider" class="browser-default">
                        <option value="0">Seleccione Proveedor</option>
                        <option value="1">Fayco</option>
                        <option value="2">GsM</option>
                        <option value="3">Swicore</option>
                    </select>
                </div>

            </div>

                <div class="row">
                    <div class="input-field col s6">
                        <label for="price">Precio</label> 
                        <input type="number" name="price" id="precio">
                    </div>

                    <div class="input-field col s6">
                        <label for="unity">Unidades</label> 
                    <input type="number" name="unity" id="precio">
                    </div>
                </div>

            <button class="btn waves-effect waves-light" type="submit">Guardar</button>

        </form>

    </div>			

</body>
</html>