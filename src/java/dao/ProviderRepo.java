package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class ProviderRepo {
    
    List<Provider> seachOrderBy(int id) {
       ConnectionCreator postgresql = ConnectionCreator.getInstance(); 
       String sql = "SELECT provider_id,name FROM provider ORDER BY provider_id = " + id + " DESC";
       List<Provider> providers = new ArrayList<>();
       try (Connection connection = postgresql.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
                while(rs.next()){
                    Provider provider = new Provider();
                    provider.setProviderId(rs.getInt("provider_id"));
                    provider.setName(rs.getString("name"));
                    providers.add(provider);
                }
                
            } catch (SQLException ex) {
            throw new RuntimeException("Fallo la obtencion de medicamentos",ex);
        }
       return providers;
    }
    
    Provider findById(int id) {

        ConnectionCreator postgresql = ConnectionCreator.getInstance(); 
        String sql = "SELECT provider_id,name FROM provider WHERE provider_id = " + id;
        try (Connection connection = postgresql.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery()) {
            Provider provider = new Provider();
            while(rs.next()){

                provider.setProviderId(rs.getInt("provider_id"));
                provider.setName(rs.getString("name"));
            }
                return provider;
            } catch (SQLException ex) {
                throw new RuntimeException("Fallo la obtencion del proveedor",ex);
            }	
       }

}
