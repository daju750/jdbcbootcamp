package dao;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
class Medicament {

  private int medicamentId;
  private String name;
  private float price;
  private int unity;
  private Date changeDate;
  private String createdBy;
  private String changedBy;
  private boolean status;
  private int version;
    
}
