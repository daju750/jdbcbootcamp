package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class ConnectionCreator {
    
    private static ConnectionCreator connection;
    private static String url;
    private static String user;
    private static String password;
    private static String dataBase;

    private ConnectionCreator() {
        user = "postgres";
        password = "wester";
        dataBase = "crud_springboot";
        url = "jdbc:postgresql://localhost:5432/" + dataBase;
    }
    
    static ConnectionCreator getInstance(){   
        if(connection == null){
            connection = new ConnectionCreator();
        }
        return connection;
    }

    final Connection getConnection(){
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
            return DriverManager.getConnection (url,user,password);
        } catch (SQLException ex) {
            throw new RuntimeException("Fallo la conexion a la base de datos",ex);
        }
    }
    
}
