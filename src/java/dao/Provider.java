package dao;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
class Provider {
     
    private Integer providerId;
    private String name;
    private Date creation_date;
    private Date change_date;
    private Date created_by;
    private String changed_by;
    private boolean status;
    private int version;
    
}
