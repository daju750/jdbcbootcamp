package dao;

import java.util.ArrayList;
import java.util.List;
import svc.ProviderDto;
import svc.ProviderService;

public class ProviderServiceImpl implements ProviderService{
    
    @Override
    public List<ProviderDto> orderBy(int id) {
        MedicamentProvider medicament = new MedicamentProviderRepo().findById(id);
        List<Provider> providers = new ProviderRepo().seachOrderBy(medicament.getProviderId());
        List<ProviderDto> providersDto = new ArrayList<>();
        providers.stream().map(
            (provider) -> new ProviderDto(
                provider.getProviderId(),
                provider.getName()
            )
        ).forEach(providersDto::add);
        
        return providersDto;
    }
    
    public ProviderDto findById(int id) {
		
        Provider provider = new ProviderRepo().findById(id);
        ProviderDto providerDto = new ProviderDto(
            provider.getProviderId(),
            provider.getName()
        );
        return providerDto;
    }
 
}
