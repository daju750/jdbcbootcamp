package dao;

import svc.MedicamentDto;
import svc.MedicamentService;

public class MedicamentServiceImpl implements MedicamentService {

    @Override
    public int save(MedicamentDto medicamentDto) {

        Medicament medicament = new Medicament();
        medicament.setMedicamentId(medicamentDto.getId());
        medicament.setName(medicamentDto.getName());
        medicament.setPrice(medicamentDto.getPrice());
        medicament.setUnity(medicamentDto.getUnity());
        
        MedicamentRepo medicamentRepo = new MedicamentRepo();
        
        if(medicamentDto.getId()==0){
            return medicamentRepo.insert(medicament);
        }else{
            medicamentRepo.update(medicament);
            return medicamentDto.getId();
        }
    }
    
    @Override
    public void delete(int id) {
        MedicamentRepo medicamentRepo = new MedicamentRepo();
        medicamentRepo.delete(id);
    }
    
    @Override
    public MedicamentDto findById(int id) {
        
        MedicamentRepo medicamentRepo = new MedicamentRepo();
        
        Medicament medicament = medicamentRepo.findById(id);
        MedicamentDto medicamentDto = new MedicamentDto(
            medicament.getMedicamentId(),
            medicament.getName(),
            medicament.getMedicamentId(),
            medicament.getPrice(),
            medicament.getUnity()
        );
        return medicamentDto;
    }

}
