package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import svc.InfoMedicaments;
import svc.Pagination;
import svc.Philtre;

class MedicamentProviderRepo {
    
     List<InfoMedicaments> seach(Pagination pagination,Philtre filter) {
        ConnectionCreator postgresql = ConnectionCreator.getInstance();
        String sql = "SELECT medicament_id,provider,name,creation_date,price,unity " +
                     "FROM info_medicaments " + 
                     "WHERE status and provider_id IN(" + filter.getProvidersId() + ") " +
                     "LIMIT " + pagination.getLimit() + " OFFSET "+pagination.getOffset();
        
        try (Connection connection = postgresql.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            List<InfoMedicaments> infoMedicaments = new ArrayList<>();
            while(rs.next()){
                InfoMedicaments medicament = new InfoMedicaments();
                medicament.setMedicamentId(rs.getInt("medicament_id"));
                medicament.setProvider(rs.getString("provider"));
                medicament.setName(rs.getString("name"));
                medicament.setCreationDate(rs.getDate("creation_date"));
                medicament.setPrice(rs.getFloat("price"));
                medicament.setUnity(rs.getInt("unity"));
                infoMedicaments.add(medicament);
             }
            return infoMedicaments;
        } catch (SQLException e) {
            throw new RuntimeException("No pudo buscar en la vista InfoMedicamento",e);
        }
    }
    
     MedicamentProvider findById(int id) {
        ConnectionCreator postgresql = ConnectionCreator.getInstance();
        String sql = "SELECT medicament_provider_id,medicament_id,provider_id "
                    +"FROM medicament_provider WHERE medicament_id = " + id;
        
        try (Connection connection = postgresql.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            MedicamentProvider medicamentProvider = new MedicamentProvider();
            while(rs.next()){
                  medicamentProvider.setProviderId(rs.getInt("provider_id"));
                  medicamentProvider.setMedicamentId(rs.getInt("medicament_id"));
                  medicamentProvider.setMedicamentProvider(rs.getInt("medicament_provider_id"));
            }
            return medicamentProvider;
        } catch (SQLException ex) {
            throw new RuntimeException("Fallo la busqueda",ex);
        }
    }
        
    void insert(MedicamentProvider medicamentProvider) {
       ConnectionCreator postgresql = ConnectionCreator.getInstance();
       String sql = "INSERT INTO medicament_provider(medicament_id,provider_id) VALUES(?,?)";

       try(Connection connection = postgresql.getConnection();
           PreparedStatement ps = connection.prepareStatement(sql)) {
           ps.setInt(1, medicamentProvider.getMedicamentId());
           ps.setInt(2, medicamentProvider.getProviderId());
           ps.executeUpdate();
       } catch (SQLException ex) {
           throw new RuntimeException("Fallo la insercion del Medicamento Proveedor",ex);
       }
    }
    
    void update(MedicamentProvider medicamentProvider) {
         ConnectionCreator postgresql = ConnectionCreator.getInstance();
         String sql = "UPDATE medicament_provider SET provider_id = ? WHERE medicament_id = ?";
         
         try(Connection connection = postgresql.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, medicamentProvider.getProviderId());
            ps.setInt(2, medicamentProvider.getMedicamentId());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            throw new RuntimeException("Fallo la actulizacion del proveedor", ex);
        }
    }
    
}
