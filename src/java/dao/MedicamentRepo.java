package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

class MedicamentRepo {
    
    int insert(Medicament medicament) {
       ConnectionCreator postgresql = ConnectionCreator.getInstance(); 
       String sql = "INSERT INTO medicament(name,price,unity)VALUES (?,?,?)";
        
       try (Connection connection = postgresql.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS)) {
            
            ps.setString(1,medicament.getName());
            ps.setFloat(2,medicament.getPrice());
            ps.setInt(3,medicament.getUnity());
            ps.executeUpdate();

           try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
               generatedKeys.next();
               return generatedKeys.getInt(1);
           }

       } catch (SQLException ex) {
           throw new RuntimeException("Fallo la insercion del medicamento",ex);
       }
    }
    
    void delete(int medicamentoId) {
        ConnectionCreator postgresql = ConnectionCreator.getInstance();
        String sql="UPDATE medicament SET status=FALSE WHERE medicament_id = ?";
            
        try (Connection connection = postgresql.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1,medicamentoId);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Fallo la eliminacion de medicamentos", ex);
        }
    }
    
    void update(Medicament medicament) {
        ConnectionCreator postgresql = ConnectionCreator.getInstance();
        String sql = "UPDATE medicament SET name = ?, price = ?, unity = ? WHERE medicament_id = ?";
        
        try(Connection connection = postgresql.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1,medicament.getName());
            ps.setFloat(2,medicament.getPrice());
            ps.setInt(3,medicament.getUnity());
            ps.setInt(4,medicament.getMedicamentId());
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new RuntimeException("Fallo la actulizacion de medicamentos",ex);
        }
    }
    
    public Medicament findById(int id){
        ConnectionCreator postgresql = ConnectionCreator.getInstance();
        String sql = "SELECT medicament_id,name,price,unity FROM Medicament m WHERE m.medicament_id = " + id;

        try (Connection connection = postgresql.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            Medicament medicament = new Medicament();
            while(rs.next()){
                medicament.setMedicamentId(rs.getInt("medicament_id"));
                medicament.setName(rs.getString("name"));
                medicament.setPrice(rs.getFloat("price"));
                medicament.setUnity(rs.getInt("unity"));
             }
            return medicament;
        } catch (SQLException ex) {
            throw new RuntimeException("Fallo la obtencion de medicamentos", ex);
        }
    }

    
}
