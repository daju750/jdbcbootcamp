package dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
class MedicamentProvider {
    
    int medicamentProvider;
    int medicamentId;
    int providerId;
    
}
