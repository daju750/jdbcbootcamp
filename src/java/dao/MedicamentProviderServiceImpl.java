package dao;

import java.util.List;
import java.util.Optional;
import svc.InfoMedicaments;
import svc.MedicamentDto;
import svc.MedicamentProviderService;
import svc.Pagination;
import svc.Philtre;

public class MedicamentProviderServiceImpl implements MedicamentProviderService {

        @Override
        public List<InfoMedicaments> find(Pagination pagination, Philtre filter) {
            
            MedicamentProviderRepo medicamentProvider = new MedicamentProviderRepo();
            return medicamentProvider.seach(pagination, filter);
        }

        
        @Override
        public void save(MedicamentDto medicamentDto) {

            Optional
                    .ofNullable(medicamentDto.getName())
                    .orElseThrow(()-> new IllegalArgumentException("Nombre nulo"));
            
            /*
            if(medicamentDto.getName()==null || medicamentDto.getName().isEmpty()){
                throw new RuntimeException("Nombre nulo");
            }*/
            
            if(medicamentDto.getProvider()==0){
                throw new RuntimeException("Provedor Nulo");
            }
            
            if(medicamentDto.getPrice()==0.0){
                throw new RuntimeException("Precio Nulo");
            }
            
            if(medicamentDto.getUnity()==0){
                throw new RuntimeException("Unidad Nulo");
            }
            
            
            MedicamentServiceImpl medicamentServImpl = new MedicamentServiceImpl();
            int medicamentId = medicamentServImpl.save(medicamentDto);
            
            MedicamentProvider medicamentProvider = new MedicamentProvider();
            medicamentProvider.setMedicamentId(medicamentId);
            medicamentProvider.setProviderId(medicamentDto.getProvider());
            
            MedicamentProviderRepo medicamentProRepo = new MedicamentProviderRepo();
            
            if(medicamentDto.getId()==0){
                medicamentProRepo.insert(medicamentProvider);
            }else{
                medicamentProRepo.update(medicamentProvider); 
            }
        }
        
    }


