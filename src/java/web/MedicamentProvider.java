package web;

import dao.MedicamentProviderServiceImpl;
import dao.MedicamentServiceImpl;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import svc.MedicamentDto;

@WebServlet(name = "medicaments-provider", urlPatterns = {"/medicaments-provider"})
public class MedicamentProvider extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        processRequest(request, response);

        MedicamentServiceImpl meicamentService = new MedicamentServiceImpl();
        meicamentService.delete(Integer.valueOf(request.getParameter("id")));
        response.sendRedirect("index.jsp");
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
             response.setContentType("text/html;charset=UTF-8");

                MedicamentProviderServiceImpl medicamentService = new MedicamentProviderServiceImpl();
                medicamentService.save(new MedicamentDto(
                      Integer.parseInt("0"),
                      request.getParameter("name"),
                      Integer.parseInt(request.getParameter("provider")),
                      Float.parseFloat(request.getParameter("price")),
                      Integer.parseInt(request.getParameter("unity"))  
                ));

            response.sendRedirect("index.jsp");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
