package web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class MedicamentDto {

    private int id;
    private String name;
    private int provider;
    private float price;
    private int unity;
    
}
