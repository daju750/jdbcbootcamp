package web;

import dao.MedicamentProviderServiceImpl;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import svc.MedicamentDto;

@WebServlet(name = "medicament", urlPatterns = {"/medicament"})
public class Medicament extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        MedicamentProviderServiceImpl medicamentProviderService = new MedicamentProviderServiceImpl();
        MedicamentDto medicamentoDto = new MedicamentDto(
                Integer.parseInt(request.getParameter("id")),
                request.getParameter("name"),
                Integer.parseInt(request.getParameter("provider")),
                Float.parseFloat(request.getParameter("price")),
                Integer.parseInt(request.getParameter("unity"))
        );
        medicamentProviderService.save(medicamentoDto);
        
        response.sendRedirect("index.jsp");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
