package svc;

public interface MedicamentService {
    
    public int save(MedicamentDto medicamentDto);
    public void delete(int id);
    public MedicamentDto findById(int id);
}
