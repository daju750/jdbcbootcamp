package svc;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class InfoMedicaments {
    
    private Integer medicamentId;
    private String provider;
    private String name;
    private Date creationDate;
    private float price;
    private Integer unity;
    
}
