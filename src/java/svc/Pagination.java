package svc;

import lombok.Getter;

@Getter
public class Pagination {

    private final int limit;
    private final int offset;
    
    public Pagination(int limit, int offset){
        
        if(limit>30){
            throw new RuntimeException("Limite de 30 registros");
        }
        
        this.limit = limit;
        this.offset = offset;
    }
    
}
