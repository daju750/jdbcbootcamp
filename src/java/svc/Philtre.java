package svc;

import java.util.List;

public class Philtre {
    
    private final List<Integer> providersId;
    
    public Philtre(List<Integer> providers){
        
        if(providers.isEmpty()){
            throw new RuntimeException("Proveedores obligatorios");
        }
        
        providersId = providers;
    }
    
    public String getProvidersId(){
        String providers  = "";
        providers = providersId
                .stream()
                .map((i) -> i+",")
                .reduce(providers, String::concat);
        return providers.substring(0, providers.length()-1);
    }
    
}
