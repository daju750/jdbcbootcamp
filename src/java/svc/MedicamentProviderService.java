package svc;

import java.util.List;

public interface MedicamentProviderService {
    
    public List<InfoMedicaments> find(Pagination pagination, Philtre filter);
    public void save(MedicamentDto medicamentoDto);
    
}
