package svc;

import java.util.List;

public interface ProviderService {

    public List<ProviderDto> orderBy(int id);
    
}
