package svc;

import java.util.Optional;

public class Person {
    private final String name;
    private final int age;

    public Person(String name,int age){
        this.name = name;
        this.age = age;
    }
    
    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }

    public Optional<Integer> getAge() {
        return Optional.ofNullable(age);
    }
    
}