package test;

import java.util.Optional;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import svc.Person;

public class PersonTest {
    
    @Test
    public void validateName() {
        Person person = new Person("", 29);
        Optional<Person> personOptional = Optional.of(person);
        
        String name = personOptional
          .flatMap(Person::getName)
          .orElse("");
        assertEquals("", name);
    }
    
}
