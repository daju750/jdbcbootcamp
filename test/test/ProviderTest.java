package test;

import dao.ProviderServiceImpl;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import svc.ProviderDto;

public class ProviderTest {
    
    @Test
    public void orderBy() {
        
        ProviderServiceImpl providerService = new ProviderServiceImpl();
        List<ProviderDto> providers = providerService.orderBy(1);
        
        assertTrue("Consulta exitosa",providers.size()>0);
    }
    
    @Test
    public void findById() {
        
        ProviderServiceImpl providerService = new ProviderServiceImpl();
        ProviderDto provider = providerService.findById(1);
        
        assertTrue("Consulta exitosa",provider.getId()>0);
    }
    
}
