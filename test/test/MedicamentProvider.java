package test;

import dao.MedicamentProviderServiceImpl;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import svc.InfoMedicaments;
import svc.MedicamentDto;
import svc.Pagination;
import svc.Philtre;

public class MedicamentProvider {
    
    @Test
    public void find() {
        
         Philtre filter = new Philtre(Arrays.asList(1, 2, 3));
         Pagination pagination = new Pagination(10,0);
         MedicamentProviderServiceImpl medicamentPro = new MedicamentProviderServiceImpl();
         List<InfoMedicaments> infoMedicaments = medicamentPro.find(pagination, filter);
         
         assertTrue("Error al consultar medicamentos",infoMedicaments.size()>0);
    }
    
    
    @Test
    public void findError() {
        
         Philtre filter = new Philtre(Arrays.asList(0));
         Pagination pagination = new Pagination(10,0);
         
         MedicamentProviderServiceImpl medicametService = new MedicamentProviderServiceImpl();
         List<InfoMedicaments> infoMedicaments = medicametService.find(pagination, filter);
         
         assertTrue("Error al consultar medicamentos",0==infoMedicaments.size());
    }
   

    @Test
    public void insertNameFail() {
        
         MedicamentDto medicamentDto = new MedicamentDto();
         medicamentDto.setPrice(200);
         medicamentDto.setUnity(5);
         medicamentDto.setProvider(1);
         Exception exception = null;
         try {
            MedicamentProviderServiceImpl medicametService = new MedicamentProviderServiceImpl();
            medicametService.save(medicamentDto);
         } catch(Exception e){
             exception = e;
         }
         
         assertEquals("Nombre nulo", exception.getMessage());
    }
    
    
    @Test
    public void insertPriceFail() {
        
         MedicamentDto medicamentDto = new MedicamentDto();
         medicamentDto.setName("Meloxicam");
         medicamentDto.setUnity(5);
         medicamentDto.setProvider(1);
         Exception exception = null;
         try {
            MedicamentProviderServiceImpl medicametService = new MedicamentProviderServiceImpl();
            medicametService.save(medicamentDto);
         } catch(Exception e){
             exception = e;
         }
         assertEquals("Precio Nulo", exception.getMessage());
    }
    
    
    @Test
    public void insertProviderFail() {
        
         MedicamentDto medicamentDto = new MedicamentDto();
         medicamentDto.setPrice(200);
         medicamentDto.setName("Meloxicam");
         medicamentDto.setUnity(5);
         Exception exception = null;
         try {
            MedicamentProviderServiceImpl medicametService =
                    new MedicamentProviderServiceImpl();
           medicametService.save(medicamentDto);
         } catch(Exception e){
             exception = e;
         }
         assertEquals("Provedor Nulo", exception.getMessage());
    }
    
    
    @Test
    public void insertUnityFail() {
        
         MedicamentDto medicamentDto = new MedicamentDto();
         medicamentDto.setPrice(200);
         medicamentDto.setName("Meloxicam");
         medicamentDto.setProvider(1);
         Exception exception = null;
         try {
            MedicamentProviderServiceImpl medicametService = new MedicamentProviderServiceImpl();
            medicametService.save(medicamentDto);
         } catch(Exception e){
             exception = e;
         }
         assertEquals("Unidad Nulo", exception.getMessage());
    }
    
    
}
